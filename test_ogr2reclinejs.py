# -*- coding: utf-8 -*-
# Big geoJSON issue tests
# Copyright Anne Ghisla a.ghisla@gmail.com
# Licensed under the GNU GPL v3 

import unittest
import os,sys 
import zipfile
from shutil import rmtree
from tempfile import mkdtemp

# probably better to add the test folder where ogr2reclinejs.py is
sys.path.append("/home/anne/src/SpazioAiDati/ogr2reclinejs")
import ogr2reclinejs as o2r

## From ingest.py.
def unzip(file, out_dir):
    zfile = zipfile.ZipFile(file)
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    for name in zfile.namelist():
        # Security feature to avoid ../../ traps
        #~ os.path.normpath(os.path.join("/", name))
        fd = open(out_dir + '/' + name, "w")
        fd.write(zfile.read(name))
        fd.close()
        
class ogr2reclinejsTestCase(unittest.TestCase):
    def setUp(self):
        ## Data from geoportale PAT http://www.territorio.provincia.tn.it
        
        ## one very complex polygon, that must be simplified. Hydrological basins
        ## 874_Bacini_idrografici_di_secondo_livello_12_12_2011.zip
        self.pathToComplexPolygon = "./idrbac3.shp"
        ## Simplifications
        ## 883_Alvei_della_PAT__pup__12_12_2011.zip
        self.pathToComplexPolygon2 = "./alvei.shp"
        ## one complex polygon that does not need simplification - comuni amministrativi
        ## 788_Comuni_amministrativi___DB_Prior_10k__12_12_2011.zip
        self.pathToSimplePolygon = "./COM_TN.shp"
        ## one complex line - curve di livello 100m. 31MB 
        ## 913_Curve_di_livello_passo_100_m__lineari__12_12_2011.zip
        self.pathToComplexLine = "./alta10l.shp"
        ## one point - punti monitoraggio qualità acqua. 
        ## 1604_Punti_monitoraggio_qualita_acqua_12_12_2011.zip
        self.pathToPoint = "./PMonit_UTM.shp"
        ## one point without projection - stazioni forestali 
        ## 1556_Stazioni_forestali_12_12_2011.zip
        self.pathToPointWithNoProjection = "./ammstf.shp"
        ## encoding errors: 
        ## 1507_Sorgente_o_pozzo_12_12_2011.zip
        self.pathToNonAsciiStringsDataset = "./sorgente_o_pozzo.shp"
        
    def tearDown(self):
        """Cleanup function, ran after asserts."""
        for computedFile in ["./idrbac3.csv", 
                             "./COM_TN.csv",
                             "./alta10l.csv", 
                             #~ "./PMonit_UTM.csv", 
                             "./ammstf.csv"]:
            if os.path.exists(computedFile):
                os.remove(computedFile)

class ogr2reclinejsConversionTestCase(ogr2reclinejsTestCase):

    def testPolygonNoProjectionConversion(self):
        with self.assertRaises(o2r.MissingProjectionException) as exc:
            polygonNoProjection = o2r.OGR2Reclinejs("./ammstf.shp")
    
    def testMissingFile(self):
        with self.assertRaises(IOError) as e:
            nodata = o2r.OGR2Reclinejs("")
    
    def testImpossibleTopologicalSimplification(self):
        with self.assertRaises(o2r.GeoJSONTooBigException) as exc:
            notTopologicallySimplifiableGeometry = o2r.OGR2Reclinejs("./alvei.shp")
            notTopologicallySimplifiableGeometry.conversion(".")
    
    def testPointConversion(self):
        points = o2r.OGR2Reclinejs("./PMonit_UTM.shp")
        points.conversion(".")
        computedCSV = open("./PMonit_UTM.csv").read()
        expectedCSV = open("./points.csv").read()
        self.assertEquals(computedCSV, expectedCSV)
    
    #@TODO finish adding tests.

class ogr2reclinejsGeoportaleTestCase(unittest.TestCase):
    def setUp(self):
        #~ grep -oe "http.*\.zip" index.html > ziplist
        #~ wget -i ../../complex-polygon/ziplist
        #~ Total wall clock time: 2h 34m 6s
        #~ Downloaded: 160 files, 773M in 2h 33m 12s (86.1 KB/s)

        self.baseFolder = "./zipfiles-geoportalePAT"
        self.tempUnzipFolder = mkdtemp(dir=self.baseFolder)
        self.convertedFolder = os.path.join(self.baseFolder, "small-converted")
        if not os.path.exists(self.convertedFolder):
            os.mkdir(self.convertedFolder)
    
    def tearDown(self):
        """ Remove all decompressed files and created CSVs and logfiles """
        rmtree(self.tempUnzipFolder)
    
    def testConversionGeoportale(self):
        """ Converts all 160 datasets, already downloaded. """
        zipfiles = os.listdir(self.baseFolder)
        #~ zipfiles = filter(lambda l: l.endswith(".zip"), zipfiles)
        zipfiles = ['883_Alvei_della_PAT__pup__12_12_2011.zip', 
                    '972_Curve_di_livello_passo_200_m__poligonali__12_12_2011.zip',
                    '../Norway.zip',
                    '1556_Stazioni_forestali_12_12_2011.zip',
                    '1507_Sorgente_o_pozzo_12_12_2011.zip']
        print len(zipfiles)
        
        failures_file = open(os.path.join(self.baseFolder, "failures"), 'wb')
        names_file = open(os.path.join(self.baseFolder, "names"), 'wb')
        
        for geodataItem in zipfiles:
            # unzip in a dedicated folder
            print "Uncompressing file: ", geodataItem
            geodataItemFolder = os.path.join(self.tempUnzipFolder, geodataItem)
            os.mkdir(geodataItemFolder)
            unzip(os.path.join(self.baseFolder, geodataItem), geodataItemFolder)
            
            files = os.listdir(geodataItemFolder)
            files = [f for f in files if f.endswith(".shp")]
                
            try:
                for onefile in files:
                    names_file.write(geodataItem + " " + onefile + "\n")
                    shp_file = os.path.join(geodataItemFolder, onefile)
                    print 'Converting file ...', shp_file
                    ogr2reclinejsInstance = o2r.OGR2Reclinejs(shp_file, 
                                                              verbose = False,
                                                              resize = True)
                    #@TODO: print the type of data
                    ogr2reclinejsInstance.conversion(self.convertedFolder)
            except o2r.MissingProjectionException:
                exceptionMessage = "Missing projection for dataset %s. No CSV generated.\n" % geodataItem
                print exceptionMessage
                failures_file.write("\n" + exceptionMessage)
            except o2r.GeoJSONTooBigException, GeoJSONexc:
                print GeoJSONexc.message, "for dataset %s" % geodataItem
                failures_file.write("\n" + GeoJSONexc.message + " Dataset %s" % geodataItem)
            #@TODO: let it write data format, number of layers
            #@TODO: calculate number of features and count the lines of the csv
        failures_file.close()
        names_file.close()
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(ogr2reclinejsConversionTestCase('testPolygonNoProjectionConversion'))
    suite.addTest(ogr2reclinejsTestCase('testPointConversion'))
    suite.addTest(ogr2reclinejsGeoportaleTestCase('testConversionGeoportale'))
    #~ suite = unittest.TestLoader().loadTestsFromTestCase(ogr2reclinejsConversionTestCase)
    return suite
    
# run it
if __name__ == '__main__':
    unittest.main()
