# -*- coding: utf-8 -*-
"""
Created on Sun Aug  5 21:22:51 2012
@author: Maurizio Napolitano <napo@fbk.eu>
@contributors: Michele Mostarda
MIT License
"""
import os,csv

#search the right version of the
try:
	from osgeo import ogr,osr
except:
	import ogr,osr
from optparse import OptionParser

MAX_CSV_FIELD_SIZE = 131072

def FeatureIterator(layer):
	feature = layer.GetNextFeature()
	while feature:
		yield feature
		feature = layer.GetNextFeature()
	return

class MissingProjectionException(Exception):
	"""Exception raised when projection information is missing in zip file."""
	pass

class GeoJSONTooBigException(Exception):
	""" Exception raised when the geometry converted to GeoJSON is too big,
		either because simplification is disabled, either because topological
		simplification did not succeed in reducing GeoJSON size.
	"""
	pass

class OGR2Reclinejs():
	
	def __init__(self,infile,verbose=False,resize=False,encoding='utf-8'):
		'''
		create a object with the source file (infile = geodata source)
		'''
		self.version = '1.1'
		self.datasource = None
		self.outfiles = []
		self.geomtypes = []
		self.geojson = 'GeoJSON'
		self.formatfound = ''
		self.sr_wgs84 = osr.SpatialReference()
		self.sr_wgs84.ImportFromEPSG(4326)
		self.verbose = verbose
		# Default encoding used to decode infiles. It is too difficult and not
		# reliable to guess encodings, not even with chardet.
		self.encoding = encoding
		self.resize = resize
		
		# check if file exists.
		if not os.path.exists(infile):
			raise IOError
		
		driver = None
		supportedformats = []
		for g in range(0,ogr.GetDriverCount()):
			supportedformats.append(ogr.GetDriver(g).GetName())
			driver= ogr.GetDriver(g)
			datasource = driver.Open(infile)
			if datasource != None:
				self.datasource = datasource
				self.formatfound = ogr.GetDriver(g).GetName()
				break

		if self.datasource is None:
			message = 'Could not find the driver for %s' % infile
			if self.verbose:
				message += "\n"
				message += "List of supported drivers:"
				for sf in supportedformats:
					message += "- %s\n" % sf
			raise Exception,message

		self.layers_fields = []
		for idx in range(0,self.datasource.GetLayerCount()):
			layer = self.datasource.GetLayer(idx)
			sr = layer.GetSpatialRef()
			if sr is None:
				raise MissingProjectionException, 'Projection not present. Impossible to generate CSV.'
			fields = {}
			self.geomtypes.append(layer.GetGeomType())
			for s in layer.schema:
				if self.verbose:
					print 'FOUND SCHEMA FIELD', s.GetName()
				nf = s.GetName()
				fields[nf.decode(self.encoding)] = s.GetTypeName()
			self.layers_fields.append(fields)
			layer.ResetReading()

	def info(self):
		'''
				return a dictionary with some information about the source file
				format => the file format name
				num_layers => the numbers of layers present in the source (allowed by the GML and Tiger format)
				layer_* => the name of the layer number *
				'''
		data = {}
		if self.datasource != None:
			#data['encoding'] = self.source_encoding
			data['format'] = self.formatfound
			data["num_layer"] =  self.datasource.GetLayerCount()
			for i in range(0,self.datasource.GetLayerCount()):
				nl = "layer_" + str(i)
				data[nl] = self.datasource.GetLayer(i).GetName()
		return data

	def metadata(self):
		'''return a list with the list of the type of fields'''
		layers_fields = []
		for i in range(0,len(self.layers_fields)):
			fields = self.layers_fields[i]
			if self.geomtypes[i] in (1,2,3):
				fields[self.geojson] = 'Geometry'
			layers_fields.append(fields)
		return layers_fields

	def outputfiles(self):
		'''return the list of the filenames for the conversion'''
		return self.outfiles
		
	def recursive_simplification(self, geometry, feature, logfile, simplification_factor):
		geometry_json = geometry.ExportToJson().encode('utf-8')
		
		fieldsize = len(geometry_json)
		# Add extra dimensions due to Excel transformation 
		fieldsize += 2 + geometry_json.count('"')
		
		if self.verbose:
			print "Obtained size of geoJSON is %d" % len(geometry_json)
		if fieldsize <= MAX_CSV_FIELD_SIZE:
			return geometry_json
		# Simplify anyway. Check if it is too costly - if so, put it back in the if.
		simplified_geometry = geometry.SimplifyPreserveTopology(simplification_factor)
		# If simplification was allowed and it worked, trigger recursion for size check.
		if self.resize is True and not simplified_geometry.Equals(geometry):
			if self.verbose:
				print "Simplifying geometry of feature %d with factor %d" % (feature.GetFID(), simplification_factor)
			logfile.write("\nSimplifying geometry of feature %d with factor %d.dddd." % (feature.GetFID(), simplification_factor))
			return self.recursive_simplification(simplified_geometry, feature, logfile, simplification_factor*2)
		# Simplification was not allowed, or did not work
		else:    
			logfile.write("\nGeoJSON too big for feature %d'. Aborting conversion." % feature.GetFID())
			raise GeoJSONTooBigException, 'GeoJSON too big for feature %d. Simplification was %s.' % (feature.GetFID(), self.resize)

	def conversion(self,destdir=None):
		'''For each layer present in the source file create a csv file with the geometry field.
		If the geometry type is a point, use the fields LAT and LON, otherwise create a GeoJSON string
		'''
		try:
			for idx in range(0,self.datasource.GetLayerCount()):
				layer = self.datasource.GetLayer(idx)
				outfile = layer.GetName() + ".csv"
				# Open file for projection errors or simplification warning
				errors_file = open(outfile + "_errors.txt", 'wb')
				if destdir is not None:
					outfile = destdir + os.sep + layer.GetName() + ".csv"
				self.outfiles.append(outfile)
				ofile = open(outfile, 'wb')
				writer = csv.writer(ofile, dialect='excel')
				head = []
				for field in self.layers_fields[idx]:
					if self.verbose:
						print 'F:[%s]' % field
					head.append(field)
				head.append(self.geojson)
				writer.writerow([s.encode("utf-8") for s in head])
				
				simplification_factor = 0.0001
				list_of_rows = []
				
				sr_source = layer.GetSpatialRef()
				
				for feature in FeatureIterator(layer):
					row = []
					for fname in self.layers_fields[idx]:
						v = feature.items().get(fname)
						#if v is None: print 'Missing value for fname', fname
						row.append(str(v))
					geometry = feature.GetGeometryRef()
					if (sr_source != self.sr_wgs84):
						ct = osr.CoordinateTransformation(sr_source, self.sr_wgs84)
						geometry.Transform(ct)
						
					# conversion
					geometry_json = self.recursive_simplification(geometry, feature, errors_file, simplification_factor)
					row.append(geometry_json)
					list_of_rows.append(row)
				# write the csv 
				writer.writerows(list_of_rows)
				# close the files
				ofile.close()
				# if the errors file is empty, erase it
				if errors_file.tell() == 0:
					os.remove(errors_file.name)
				else:
					errors_file.close()
				layer.ResetReading()
				
		except OSError as e:
			print e

def main():
	usage = "%prog [options] arg\n"
	usage += " version %s " % OGR2Reclinejs.version

	parser = OptionParser(usage)
	#FIXME parser.add_option("-o","--ouput",action="store",dest="output",help="output file\nif not specified is used the layer name with the csv extension")
	parser.add_option("-d", "--destinatiodir", action="store", dest="destdir", help="output directory - default is current directory, the name is the layer name with the .csv suffix",default=None)
	parser.add_option("-e","--encoding",action="store",dest="encoding",help="Charachter encoding source data - default=utf-8",default="utf-8")
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="verbose output",default=False)
	parser.add_option("-r","--resize",action="store_true",dest="resize",help="resize or skip the geometry size. Be carefull: this CHANGE the data!")
	parser.add_option("-I","--info",action="store_true",dest="info",help="show info file, you need to use also the -i parameter (tip: with the command ogrinfo you obtain more informations)",default=False)

	(options,args) = parser.parse_args()

	if len(args) == 0:
		parser.print_help()
	else:
		inputfile = args[0]
		v = False
		r = False
		if options.verbose:
			v = True
		if options.resize:
			r = True
		ogr2reclinejs = OGR2Reclinejs(inputfile,v,r,options.encoding)
		ogr2reclinejs.conversion(options.destdir)
		if v:
			idx = 0
			info = ogr2reclinejs.info()
			metadata = ogr2reclinejs.metadata()
			for f in ogr2reclinejs.outputfiles():
				print "file %s" % f
				#print "charachter encoding = %s" % info['encoding']
				print "\tfields for %s:" % f
				mt = metadata[idx]
				for m in mt:
					print "\t\t%s => %s" % (m,mt[m])
				idx += 1

if __name__ == "__main__":
	main()
